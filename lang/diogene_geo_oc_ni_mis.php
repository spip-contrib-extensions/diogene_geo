<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_geo?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Adressa',

	// G
	'gis_info_descriptif' => 'Descritiéu dóu pounch',
	'gis_info_titre' => 'Titre dóu pounch',

	// L
	'label_geo_cacher' => 'En mancança, escoundre la carta',
	'label_geo_forcer_existant' => 'Fourçà la selecioun d’un pounch',
	'label_recherche' => 'Afichà un camp de recercà souta la carta.',
	'latitude' => 'Latituda',
	'longitude' => 'Loungituda',

	// M
	'message_article_geolocaliser' => 'Geoloucalisà aquela pàgina',

	// N
	'non_localisable' => 'Noun loucalisable',
	'non_localisable_case' => 'Aqueu media noun es loucalisable',

	// R
	'recherche' => 'Recerca'
);
