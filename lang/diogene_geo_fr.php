<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_geo.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Adresse',

	// F
	'form_legend' => 'Informations géographiques',

	// G
	'gis_info_descriptif' => 'Descriptif du point',
	'gis_info_titre' => 'Titre du point',

	// L
	'label_geo_afficher' => 'Afficher les informations de géolocalisation',
	'label_geo_cacher' => 'Cacher la carte par défaut',
	'label_geo_cacher_defaut' => 'Cache la carte et les champs de géolocalisation par défaut',
	'label_geo_forcer_existant' => 'Forcer la sélection d’un point',
	'label_geo_forcer_existant_defaut' => 'Imposer le choix d’un point géolocalisé préexistant sur le site (n’affichera pas de carte)',
	'label_recherche' => 'Afficher un champ de recherche sous la carte.',
	'label_supprimer_gis' => 'Supprimer les informations géographiques liées',
	'latitude' => 'Latitude',
	'longitude' => 'Longitude',

	// M
	'message_article_geolocaliser' => 'Géolocaliser cette page',
	'message_postion_auto' => 'Votre média a été positionné automatiquement selon ses métadonnées.',

	// N
	'non_localisable' => 'Non localisable',
	'non_localisable_case' => 'Ce média n’est pas localisable',

	// R
	'recherche' => 'Recherche',

	// Z
	'zoom' => 'Zoom'
);
