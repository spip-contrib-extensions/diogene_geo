<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_geo.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_geo_description' => 'Ajoute la possibilité de sélectionner une position géographique d’un objet depuis son formulaire d’édition si cette option est activée dans le masque de formulaire de Diogène',
	'diogene_geo_nom' => 'Diogène - Géo',
	'diogene_geo_slogan' => 'Complément géographique pour "Diogène"'
);
