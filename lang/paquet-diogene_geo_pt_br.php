<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_geo?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_geo_description' => 'Incluir a possibilidade de selecionar a posição geográfica de um objeto a partir do seu formulário de edição, se esta opção estiver ativa na máscara de formulário do Diogenes',
	'diogene_geo_nom' => 'Diogenes - Geo',
	'diogene_geo_slogan' => 'Complemento geográfico para o "Diogenes"'
);
