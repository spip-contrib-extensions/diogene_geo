<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_geo?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Endereço',

	// F
	'form_legend' => 'Informações geográficas',

	// G
	'gis_info_descriptif' => 'Descrição do ponto',
	'gis_info_titre' => 'Título do ponto',

	// L
	'label_geo_afficher' => 'Exibir as informações de geolocalização',
	'label_geo_cacher' => 'Ocultar o mapa por padrão',
	'label_geo_cacher_defaut' => 'Ocultar o mapa e os campos de geolocalização por padrão',
	'label_geo_forcer_existant' => 'Forçar a seleção de um ponto',
	'label_geo_forcer_existant_defaut' => 'Impor a escolha de um ponto geolocalizado preexistente no site (não exibirá o mapa)',
	'label_recherche' => 'Exibir um campo de busca no mapa.',
	'label_supprimer_gis' => 'Excluir as informações geográficas vinculadas',
	'latitude' => 'Latitude',
	'longitude' => 'Longitude',

	// M
	'message_article_geolocaliser' => 'Geolocalizar esta página',
	'message_postion_auto' => 'A sua mídia foi posicionada automaticamente, de acordo com os seus metadados',

	// N
	'non_localisable' => 'Não localizável',
	'non_localisable_case' => 'Esta mídia não é localizável',

	// R
	'recherche' => 'Busca',

	// Z
	'zoom' => 'Zoom'
);
