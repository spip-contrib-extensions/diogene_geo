<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_geo?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Address',

	// F
	'form_legend' => 'Geographic datas',

	// G
	'gis_info_descriptif' => 'Description of point',
	'gis_info_titre' => 'Title of the point',

	// L
	'label_geo_afficher' => 'View location information',
	'label_geo_cacher' => 'Hide the map by default',
	'label_geo_cacher_defaut' => 'Hide the map and the geolocating fields by default',
	'label_geo_forcer_existant' => 'Force the selection of a point',
	'label_geo_forcer_existant_defaut' => 'Impose the choice of a pre-existing geolocalised point on the site (will not show a map)',
	'label_recherche' => 'Show a search field under the map.',
	'label_supprimer_gis' => 'Delete the related geographical informations',
	'latitude' => 'Latitude',
	'longitude' => 'Longitude',

	// M
	'message_article_geolocaliser' => 'Geolocate this page',
	'message_postion_auto' => 'Your media has been automatically positionned according to its metadata.',

	// N
	'non_localisable' => 'Not localizable',
	'non_localisable_case' => 'This media can’t be geolocated',

	// R
	'recherche' => 'Search',

	// Z
	'zoom' => 'Zoom'
);
