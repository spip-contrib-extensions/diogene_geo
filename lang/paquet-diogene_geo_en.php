<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_geo?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_geo_description' => 'Enable to select a geographic position of an object from its edit form if this option is enabled in the mask form of "Diogene"',
	'diogene_geo_nom' => 'Diogene - Geo',
	'diogene_geo_slogan' => 'Geolocation add-on for "Diogene"'
);
