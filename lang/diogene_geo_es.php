<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_geo?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Dirección',

	// F
	'form_legend' => 'Informaciones geográficas',

	// G
	'gis_info_descriptif' => 'Descriptivo del punto',
	'gis_info_titre' => 'Título del punto',

	// L
	'label_geo_afficher' => 'Mostrar la información de geolocalización',
	'label_geo_cacher' => 'Ocultar el mapa por defecto',
	'label_geo_cacher_defaut' => 'Esconde el mapa y los campos de geolocalización por defecto',
	'label_geo_forcer_existant' => 'Forzar la selección de un punto',
	'label_geo_forcer_existant_defaut' => 'Imponer la elección de un punto geolocalizado preexistente en el sitio (no mostrará el mapa)',
	'label_recherche' => 'Mostrar un campo de búsqueda debajo del mapa.',
	'label_supprimer_gis' => 'Eliminar informaciones geográficas vinculadas',
	'latitude' => 'Latitud',
	'longitude' => 'Longitud',

	// M
	'message_article_geolocaliser' => 'Geolocalizar esta página',
	'message_postion_auto' => 'Su medio se ha configurado automáticamente de acuerdo a sus metadatos.',

	// N
	'non_localisable' => 'No localizable',
	'non_localisable_case' => 'Este medio no es localizable',

	// R
	'recherche' => 'Búsqueda',

	// Z
	'zoom' => 'Zoom'
);
